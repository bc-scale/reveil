using System;
using System.Net;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Reveil.Backend.BusinessLogic;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Repositories;
using Reveil.Backend.Structures;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.CaseBl {
  public class OpenCaseTests {
    private readonly INotificationSenderFactory _devNullSenderFactory;
    public OpenCaseTests() {
      _devNullSenderFactory = new NotificationSenderFactory(Options.Create(new NotificationConfiguration()));
    }
    
    [Theory]
    [InlineData(RepoType.InMemory)]
    [InlineData(RepoType.FilePersisted)]
    public void TestOpenCaseIfCaseAlreadyExistsReturns409ErrorResponse(RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      cases.Add(new CaseEntity {Id = "0", AuthorizationToken = "HelloWorld", CreationDate = DateTime.UtcNow, State = CaseState.Open});
      var bl = new CasesBl(cases, journals, new GenericInMemoryRepository<MessageEntity>(), _devNullSenderFactory);
      var r = bl.OpenCase("0", "HelloWorld");
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.Conflict);

      cases.Remove(_ => true);
      journals.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestOpenNewCaseAddsEntryToRepoAddsJournalAndReturns201CreatedResponse(string caseId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var bl = new CasesBl(cases, journals, new GenericInMemoryRepository<MessageEntity>(), _devNullSenderFactory);

      var r = bl.OpenCase(caseId, "");

      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<SuccessResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.Created);
      cases.Get(caseId).Value.GetValueOrDefault().Id.Should().Be(caseId);

      journals
        .GetAll(c => c.CaseId == caseId).Value
        .Should().ContainSingle(c => c.JournalType == JournalEntryType.CaseOpenedByCreator);

      cases.Remove(_ => true);
      journals.Remove(_ => true);
    }
  }
}
