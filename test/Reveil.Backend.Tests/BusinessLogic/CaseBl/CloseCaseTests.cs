using System.Net;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Reveil.Backend.BusinessLogic;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Structures;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.CaseBl {
  public class CloseCaseTests {
    private readonly INotificationSenderFactory _devNullSenderFactory;
    public CloseCaseTests() {
      _devNullSenderFactory = new NotificationSenderFactory(Options.Create(new NotificationConfiguration()));
    }
    
    [Theory]
    [InlineData(RepoType.InMemory)]
    [InlineData(RepoType.FilePersisted)]
    public void TestCloseNotExistingTestReturns404ErrorResponse(RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var bl = new CasesBl(cases, journals, messages, _devNullSenderFactory);
      var r = bl.CloseCase("0");
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestCloseExistingCaseAddsJournalAndReturns200CreatedResponse(string caseId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      cases.Add(new CaseEntity {Id = caseId});
      var bl = new CasesBl(cases, journals, messages, _devNullSenderFactory);
      var r = bl.CloseCase(caseId);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<SuccessResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      cases.Get(caseId).Value.GetValueOrDefault().Id.Should().Be(caseId);
      journals
        .GetAll(c => c.CaseId == caseId).Value
        .Should().ContainSingle(c => c.JournalType == JournalEntryType.CaseClosedByCreator || c.JournalType == JournalEntryType.CaseClosedByManagement);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
    }
  }
}
