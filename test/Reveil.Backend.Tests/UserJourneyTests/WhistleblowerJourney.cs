using System.Linq;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Reveil.Backend.BusinessLogic;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Requests;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.UserJourneyTests {
  public class WhistleblowerJourney {
    private readonly INotificationSenderFactory _devNullSenderFactory;
    public WhistleblowerJourney() {
      _devNullSenderFactory = new NotificationSenderFactory(Options.Create(new NotificationConfiguration()));
    }
    
    [Theory]
    [InlineData(RepoType.InMemory)]
    [InlineData(RepoType.FilePersisted)]
    public void ExistenceCheckOfWhistleblowerCreatedContent(RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var casesBl = new CasesBl(cases, journals, messages, _devNullSenderFactory);
      var journalBl = new JournalBl(cases, journals);
      var messagesBl = new MessagesBl(cases, journals, messages, files, _devNullSenderFactory);

      casesBl.OpenCase("1", "auth").IsSuccess.Should().BeTrue();
      journalBl.GetJournalEntriesByCase("2").IsFailure.Should().BeTrue(); // Wrong case
      var journalsResult = journalBl.GetJournalEntriesByCase("1");
      journalsResult.IsSuccess.Should().BeTrue();
      journalsResult.Value.Count.Should().Be(1);

      journalBl.GetJournalEntry("2", journalsResult.Value.Ids.Single()).IsFailure.Should().BeTrue(); // wrong Case
      journalBl.GetJournalEntry("1", journalsResult.Value.Ids.Single()).IsSuccess.Should().BeTrue(); // right Case

      var message = new CreateMessageRequest {Data = "Gulasch", DataProcessingMethodHint = 0, PublicKey = "Hello"};
      messagesBl.CreateMessage("2", "1", message).IsFailure.Should().BeTrue(); // Not Existing Case
      messagesBl.CreateMessage("1", "1", message).IsSuccess.Should().BeTrue(); // now correct Case

      messagesBl.GetMessage("2", "1").IsFailure.Should().BeTrue();
      messagesBl.GetMessage("1", "1").IsSuccess.Should().BeTrue();

      var fileUpload = new FileUploadRequest {Data = "a", DataProcessingMethodHint = 0, FileName = "file.ext", PublicKey = "Hello"};
      messagesBl.UploadFile("2", "1", "1", fileUpload).IsFailure.Should().BeTrue(); // Wrong case
      messagesBl.UploadFile("1", "2", "1", fileUpload).IsFailure.Should().BeTrue(); // Wrong message
      messagesBl.UploadFile("1", "1", "1", fileUpload).IsSuccess.Should().BeTrue(); // now correct Upload

      messagesBl.GetFiles("2", "1").IsFailure.Should().BeTrue(); // wrong case
      messagesBl.GetFiles("1", "2").IsFailure.Should().BeTrue(); // wrong message
      messagesBl.GetFiles("1", "1").IsSuccess.Should().BeTrue();

      messagesBl.GetFileContent("2", "1", "1").IsFailure.Should().BeTrue(); // wrong case
      messagesBl.GetFileContent("1", "2", "1").IsFailure.Should().BeTrue(); // wrong message
      messagesBl.GetFileContent("1", "1", "2").IsFailure.Should().BeTrue(); // wrong file
      messagesBl.GetFileContent("1", "1", "1").IsSuccess.Should().BeTrue();

      messagesBl.GetFileMetadata("2", "1", "1").IsFailure.Should().BeTrue(); // wrong case
      messagesBl.GetFileMetadata("1", "2", "1").IsFailure.Should().BeTrue(); // wrong message
      messagesBl.GetFileMetadata("1", "1", "2").IsFailure.Should().BeTrue(); // wrong file
      messagesBl.GetFileMetadata("1", "1", "1").IsSuccess.Should().BeTrue();

      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }
  }
}
