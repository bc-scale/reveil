using System.Net;
using CSharpFunctionalExtensions;
using FluentAssertions;
using Reveil.Backend.Extensions;
using Xunit;

namespace Reveil.Backend.Tests.Extensions {
  public class OrErrorResponseExtensionsTests {
    [Fact]
    public void TestOrServerErrorExtension() {
      const string Message = "failure";
      var r = Result.Failure<string>(Message)
        .OrServerError();

      r.IsFailure.Should().BeTrue();
      r.IsSuccess.Should().BeFalse();
      r.Error.IsSuccess.Should().BeFalse();
      r.Error.Error.Should().Contain(Message);
      r.Error.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
    }

    [Fact]
    public void TestOrNotFoundExtensionInMaybeNoneCase() {
      var r = Result.Success(Maybe<string>.None)
        .OrServerError()
        .OrNotFound();

      r.IsFailure.Should().BeTrue();
      r.IsSuccess.Should().BeFalse();
      r.Error.IsSuccess.Should().BeFalse();
      r.Error.Error.Should().Contain("not found");
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }

    [Fact]
    public void TestMapValueOrNotFoundInMaybeSomeCase() {
      const string Value = "Hello";
      var r = Result.Success(Maybe<string>.From(Value))
        .OrServerError()
        .MapValueOrNotFound();

      r.IsSuccess.Should().BeTrue();
      r.IsFailure.Should().BeFalse();
      r.Value.Should().Be(Value);
    }
  }
}
