using System;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Novell.Directory.Ldap;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models;
using Reveil.Backend.Models.Configuration.Authentication;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Authentication {
  public class LdapAuthenticationProvider : IAuthenticationProvider {
    private readonly string[] _ldapUserAttributes;
    private readonly LdapAuthenticationProviderConfiguration _configuration;

    public LdapAuthenticationProvider(LdapAuthenticationProviderConfiguration configuration) {
      _configuration = configuration;
      _ldapUserAttributes = new[] {
        _configuration.DisplayNameAttribute,
        _configuration.FirstNameAttribute,
        _configuration.LastNameAttribute,
        _configuration.UserIdAttribute,
        _configuration.MailAttribute
      };
    }

    public Task<Result<Maybe<UserEntity>>> GetUser(string userId) {
      var ldapConn = new LdapConnection();
      try {
        ldapConn.Connect(_configuration.LdapServer, _configuration.LdapServerPort);
        var dcs = string.Join(",", _configuration.DomainComponents!.Select(dc => $"dc={dc}"));
        var bindResult = _configuration.AllowedOUs!
          .Select(ou => $"cn={_configuration.LdapAdminUsername},ou={ou},{dcs}")
          .Select(dn => TryBind(ldapConn, dn, _configuration.LdapAdminPassword!))
          .FirstOrDefault();

        if (!bindResult)
          return Task.FromResult(Result.Failure<Maybe<UserEntity>>("Could not authenticate with Admin Ldap User against Ldap Server"));

        var searchResults = _configuration.AllowedOUs!
          .SelectMany(ou =>
            ldapConn.Search($"ou={ou},{dcs}", LdapConnection.ScopeSub, $"({_configuration.UserIdAttribute}={userId})", _ldapUserAttributes, false))
          .ToList();

        if (!searchResults.Any())
          return Task.FromResult(Result.Success(Maybe<UserEntity>.None));

        var userEntry = Maybe.From(searchResults.Select(Map).FirstOrDefault()!);
        return Task.FromResult(Result.Success(userEntry));
      } catch (Exception exc) {
        return Task.FromResult(Result.Failure<Maybe<UserEntity>>(exc.Message));
      }
    }

    public Task<Result<bool>> AuthenticateAsync(UserEntry entry) {
      return Task.FromResult(AuthenticateCrossPlatform(entry));
    }

    private UserEntity Map(LdapEntry ldapEntry) {
      return new UserEntity {
        Firstname = GetAttribute(ldapEntry, _configuration.FirstNameAttribute),
        LastName = GetAttribute(ldapEntry, _configuration.LastNameAttribute),
        Mail = GetAttribute(ldapEntry, _configuration.MailAttribute),
        Id = GetAttribute(ldapEntry, _configuration.UserIdAttribute),
        Phone = GetAttribute(ldapEntry, _configuration.PhoneNumberAttribute, "+00"),
        DisplayName = GetAttribute(ldapEntry, _configuration.DisplayNameAttribute),
      };
    }

    private static string GetAttribute(LdapEntry entry, string attribute, string defaultValue = null) {
      try {
        return entry.GetAttribute(attribute).StringValue;
      } catch {
        return defaultValue;
      }
    }

    private Result<bool> AuthenticateCrossPlatform(UserEntry entry) {
      var ldapConn = new LdapConnection();
      try {
        ldapConn.Connect(_configuration.LdapServer, _configuration.LdapServerPort);
        var dcs = string.Join(",", _configuration.DomainComponents!.Select(dc => $"dc={dc}"));
        var authResults = _configuration.AllowedOUs!
          .Select(ou => $"cn={entry.Username},ou={ou},{dcs}")
          .Select(dn => TryBind(ldapConn, dn, entry.Password))
          .ToList();

        return Result.Success(authResults.Any(successfulResult => successfulResult));
      } catch (Exception exc) {
        return Result.Failure<bool>(exc.Message);
      }
    }

    private static bool TryBind(ILdapConnection ldapConnection, string dn, string password) {
      try {
        ldapConnection.Bind(dn, password);
        return true;
      } catch {
        return false;
      }
    }
  }
}
