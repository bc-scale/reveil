using System;
using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Repositories {
  public class GenericInMemoryRepository<T> : IGenericRepository<T> where T : BaseEntity {
    private readonly List<T> _inMemoryStorage = new();

    public Result<T> Add(T entity) {
      _inMemoryStorage.Add(entity);
      return Result.Success(entity);
    }

    public Result<Maybe<T>> Get(string id) =>
      Result.Success(Maybe.From(_inMemoryStorage.FirstOrDefault(c => c.Id == id)!));

    public Result<Maybe<T>> Get(Predicate<T> predicate) =>
      Result.Success(Maybe.From(_inMemoryStorage.FirstOrDefault(e => predicate(e))!));

    public Result<IList<T>> GetAll(Predicate<T> predicate) =>
      Result.Success<IList<T>>(_inMemoryStorage.Where(e => predicate(e)).ToList());

    public Result Remove(Predicate<T> predicate) {
      var elem = _inMemoryStorage.FirstOrDefault(p => predicate(p));
      if (elem == null) return Result.Success();

      _inMemoryStorage.Remove(elem);
      return Result.Success();
    }

    public Result Remove(string id) {
      return Remove(c => c.Id == id);
    }
  }
}
