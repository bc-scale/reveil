using System.Collections.Generic;
using Reveil.Backend.Models.Api.Responses.Primitives;

namespace Reveil.Backend {
  public static class Constants {
    public static readonly List<MediaType> MediaTypes = new() {
      new MediaType("png", "Portable Network Graphic"),
      new MediaType("jpg", "Joint Photographic Experts Group (JPEG) Graphic"),
      new MediaType("jpeg", "Joint Photographic Experts Group (JPEG) Graphic"),
      new MediaType("gif", "Graphics Interchange Format"),
      new MediaType("svg", "Scalable Vector Graphic"),
      new MediaType("bmp", "Bitmap Graphic"),
      new MediaType("dib", "Bitmap Graphic"),
      new MediaType("pdf", "Portable Document Format"),
      new MediaType("xslx", "Excel Spreadsheet File Format"),
      new MediaType("odf", "Open Document Foundation Format"),
      new MediaType("pptx", "Powerpoint"),
      new MediaType("ppt", "Powerpoint"),
      new MediaType("doc", "Microsoft Word Document"),
      new MediaType("docx", "Microsoft Word Document"),
      new MediaType("rtf", "Rich Text Format"),
      new MediaType("xml", "eXtensible Markup Language File"),
      new MediaType("json", "JavaScript Object Notation File"),
      new MediaType("yaml", "Yet another Markup Language (YAML) File"),
      new MediaType("mp4", "MPEG-4 digital multimedia container format"),
      new MediaType("m4a", "MPEG-4 digital multimedia container format"),
      new MediaType("m4b", "MPEG-4 digital multimedia container format"),
      new MediaType("m4r", "MPEG-4 digital multimedia container format"),
      new MediaType("m4p", "MPEG-4 digital multimedia container format"),
      new MediaType("m4v", "MPEG-4 digital multimedia container format"),
      new MediaType("webm", "VP9 Container Format for Video"),
      new MediaType("opus", "OPUS Audio File"),
      new MediaType("vorbis", "Vorbis Audio File"),
      new MediaType("ogg", "Xiph Org Media Format"),
      new MediaType("mp3", "Audio File"),
      new MediaType("mkv", "Matroska"),
      new MediaType("mov", "Movie Format")
    };
  }
}
