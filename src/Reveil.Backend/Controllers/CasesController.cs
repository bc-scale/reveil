using System.Net;
using Microsoft.AspNetCore.Mvc;
using Reveil.Backend.Extensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Middleware.Filter;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Structures;

namespace Reveil.Backend.Controllers {
  [ApiController]
  [Route("[controller]")]
  [Produces("application/json")]
  [Consumes("application/json")]
  public class CaseController : ReveilBaseController {
    private readonly ICasesBl _businessLogic;

    public CaseController(ICasesBl businessLogic) {
      _businessLogic = businessLogic;
    }

    /// <summary>
    /// Gets all Cases created (requires Admin authentication)
    /// </summary>
    /// <returns></returns>
    [HttpGet("/cases", Name = nameof(GetAllCases))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(ChildrenListResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetAllCases() =>
      _businessLogic.GetAllCases().MatchAndSetStatusCode(HttpContext);


    /// <summary>
    /// Returns a case by its given Id
    /// </summary>
    /// <param name="caseId">a random Identifier for the new case</param>
    /// <returns></returns>
    [HttpGet("/cases/{caseId}", Name = nameof(GetCase))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(CaseResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetCase(string caseId) =>
      _businessLogic.GetCase(caseId).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Creates a new case
    /// </summary>
    /// <param name="caseId">a random Identifier for the new case</param>
    /// <returns></returns>
    [HttpPost("/cases/{caseId}", Name = nameof(OpenCase))]
    [ProducesResponseType(typeof(SuccessResponse), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Conflict)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse OpenCase(string caseId) =>
      _businessLogic.OpenCase(caseId, GetAuthorizationToken()).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Updates the State of a Case (Admin only)
    /// </summary>
    /// <param name="caseId">a random Identifier for the new case</param>
    /// <param name="caseState">the State the case should be updated with</param>
    /// <returns></returns>
    [HttpPatch("/cases/{caseId}", Name = nameof(UpdateCaseState))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(SuccessResponse), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Conflict)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse UpdateCaseState(string caseId, CaseState caseState) =>
      _businessLogic.UpdateCaseState(caseId, caseState).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Deletes an existing case (for Development-Environment only)
    /// </summary>
    /// <param name="caseId">a random Identifier for the new case</param>
    /// <returns></returns>
    [HttpDelete("/cases/{caseId}", Name = nameof(DeleteCase))]
    [RequireAuthorization]
    [DevelopmentEnvironment]
    [ProducesResponseType(typeof(SuccessResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse DeleteCase(string caseId) =>
      _businessLogic.DeleteCase(caseId).MatchAndSetStatusCode(HttpContext);
  }
}
