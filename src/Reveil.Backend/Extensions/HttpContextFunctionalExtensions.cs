using System;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Http;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Extensions {
  public static class HttpContextFunctionalExtensions {
    public static BaseResponse MatchAndSetStatusCode<TSuccess>(this Result<TSuccess, ErrorResponse> result, HttpContext context = null)
      where TSuccess : BaseResponse {
      var r = result.Match(s => (BaseResponse)s, e => e);
      if (context == null) return r;

      try {
        context.Response.StatusCode = (int)r.StatusCode;
      } catch (NullReferenceException) {
        /* ignored */
      }

      return r;
    }
  }
}
