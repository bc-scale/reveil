// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Reveil.Backend.Models.Api.Requests {
  public record AuthenticationRequest {
    public string Username { get; set; }
    public string Password { get; set; }
    public string PublicKey { get; set; }
    public ulong Nonce { get; set; }
  }
}
