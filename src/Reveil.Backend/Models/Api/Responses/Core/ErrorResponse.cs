// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;
using System.Net;

namespace Reveil.Backend.Models.Api.Responses.Core {
  public class ErrorResponse : BaseResponse {
    public string Error { get; set; }

    public static ErrorResponse Create(HttpStatusCode code, string errorMessage) {
      return new ErrorResponse {
        IsSuccess = false,
        Error = errorMessage,
        StatusCode = code,
        DateTime = DateTime.UtcNow
      };
    }
  }
}
