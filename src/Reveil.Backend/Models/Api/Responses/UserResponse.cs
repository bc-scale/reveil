// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System.Net;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Models.Api.Responses {
  public class UserResponse : SuccessResponse {
    public string Id { get; set; }
    public string Name { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }

    public static UserResponse Create(UserEntity entity, HttpStatusCode statusCode, string message = null) {
      var r = Create(statusCode, message);
      return new UserResponse {
        Id = entity.Id,
        Name = entity.DisplayName,
        Email = entity.Mail,
        FirstName = entity.Firstname,
        LastName = entity.LastName,
        Phone = entity.Phone,
        Message = r.Message,
        DateTime = r.DateTime,
        IsSuccess = r.IsSuccess,
        StatusCode = r.StatusCode
      };
    }
  }
}
