// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;
using System.Net;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Models.Api.Responses {
  public class FileMetadataResponse : SuccessResponse {
    public string FileId { get; set; }
    public string CaseId { get; set; }
    public string MessageId { get; set; }

    public string UploaderUserId { get; set; }
    public string FileName { get; set; }
    public DateTime UploadDate { get; set; }

    public static FileMetadataResponse Create(FileEntity e, HttpStatusCode statusCode, string message = null) {
      var r = Create(statusCode, message);
      return new FileMetadataResponse {
        FileId = e.Id,
        CaseId = e.CaseId,
        MessageId = e.MessageId,
        FileName = e.FileName,
        UploadDate = e.UploadDate,
        UploaderUserId = e.UserId,
        Message = r.Message,
        DateTime = r.DateTime,
        IsSuccess = r.IsSuccess,
        StatusCode = r.StatusCode
      };
    }
  }
}
