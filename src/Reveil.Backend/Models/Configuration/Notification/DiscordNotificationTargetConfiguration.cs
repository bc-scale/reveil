// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

using System.Collections.Generic;

namespace Reveil.Backend.Models.Configuration.Notification {
  public class DiscordNotificationTargetConfiguration : WebhookNotificationTargetConfiguration {
    public bool AllowMentionRoles { get; set; } = false;
    public bool AllowMentionEveryone { get; set; } = false;
    public bool AllowMentionEveryoneOnline { get; set; } = false;
    
    public bool MentionEveryone { get; set; } = false;
    public bool MentionEveryoneOnline { get; set; } = false;
    public List<string> CustomMentions { get; set; } = new();
    
    public string AvatarUrl { get; set; } = "https://gitlab.com/reveil/reveil/-/raw/main/docs/assets/puzzle.png";
    public string CustomMessage { get; set; } = "";
  }
}
