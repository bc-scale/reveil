namespace Reveil.Backend.Models.Configuration.Notification {
  public class NotificationTargetConfigurationBase {
    public string Type { get; set; }
  }
}
