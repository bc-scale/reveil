namespace Reveil.Backend.Models.Configuration.Notification {
  public class WebhookNotificationTargetConfiguration : NotificationTargetConfigurationBase {
    public string WebhookUrl { get; set; }
  }
}
