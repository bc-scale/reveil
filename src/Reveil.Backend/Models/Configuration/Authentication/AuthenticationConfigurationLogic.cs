using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;

namespace Reveil.Backend.Models.Configuration.Authentication {
  public static class AuthenticationConfigurationLogic {
    public static void ConfigureAuthenticationConfiguration(IConfiguration configuration, AuthenticationConfiguration opts) {
      var configuredProviderConfigurations = new List<AuthenticationProviderConfigurationBase>();
      var availableProviderConfigurationTypes = GetAvailableAuthenticationProviderConfigurationTypes();

      for (var index = 0;; index++) {
        var providerConfigurationPathWithIndex = $"Authentication:Providers:{index}";
        var userConfiguredProviderType = configuration.GetValue<string>($"{providerConfigurationPathWithIndex}:Type");
        if (userConfiguredProviderType == null) // we reached the end. no further Providers configured
          break;

        var correspondingProviderType = availableProviderConfigurationTypes.GetAuthenticationProviderConfigurationTypeByName(userConfiguredProviderType);
        if (correspondingProviderType == null) break;

        var t = (AuthenticationProviderConfigurationBase)configuration
          .GetSection(providerConfigurationPathWithIndex)
          .Get(correspondingProviderType);
        configuredProviderConfigurations.Add(t);
      }

      opts.Providers = configuredProviderConfigurations;
    }

    public static List<Type> GetAvailableAuthenticationProviderConfigurationTypes() {
      var availableAuthenticationProviderConfigurations = Assembly
        .GetExecutingAssembly()
        .GetTypes()
        .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(AuthenticationProviderConfigurationBase)))
        .ToList();
      return availableAuthenticationProviderConfigurations;
    }

    public static Type GetAuthenticationProviderConfigurationTypeByName(this IEnumerable<Type> availableProviderConfigurationTypes, string providerName) {
      return availableProviderConfigurationTypes.SingleOrDefault(t => IsMatchingAuthenticationProviderConfigurationType(providerName, t));
    }
    
    private static bool IsMatchingAuthenticationProviderConfigurationType(string providerName, MemberInfo providerType) {
      if (providerType == null)
        return false;
      
      var configTypeName = providerType.Name
        .Replace(nameof(AuthenticationProviderConfigurationBase), "")
        .Replace(nameof(AuthenticationProviderConfigurationBase).Replace("Base", ""), "");
      
      return string.Equals(providerName, configTypeName, StringComparison.InvariantCultureIgnoreCase)
        || string.Equals(providerName, providerType.Name, StringComparison.InvariantCultureIgnoreCase);
    }
  }
}
