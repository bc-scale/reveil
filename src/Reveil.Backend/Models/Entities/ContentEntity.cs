namespace Reveil.Backend.Models.Entities {
  public class ContentEntity : BaseEntity {
    public string Language { get; set; }
    public string Content { get; set; }
    public string Type { get; set; }
  }
}
