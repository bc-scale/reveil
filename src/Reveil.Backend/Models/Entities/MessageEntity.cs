using System;

namespace Reveil.Backend.Models.Entities {
  public class MessageEntity : BaseEntity {
    public string CaseId { get; set; }
    public string Data { get; set; }
    public string AuthorId { get; set; }
    public DateTime CreationDate { get; set; }
    public int DataProcessingMethodHint { get; set; }
  }
}
