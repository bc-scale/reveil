namespace Reveil.Backend.Models.Entities {
  public class UserEntity : BaseEntity {
    public string Firstname { get; set; }
    public string LastName { get; set; }
    public string DisplayName { get; set; }
    public string Mail { get; set; }
    public string Phone { get; set; }
  }
}
