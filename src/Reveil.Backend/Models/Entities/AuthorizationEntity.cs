using System;

namespace Reveil.Backend.Models.Entities {
  public class AuthorizationEntity : BaseEntity {
    public string UserId { get; set; }
    public string AuthorizationToken { get; set; }
    public DateTime ValidTo { get; set; }
  }
}
