// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Reveil.Backend.Models {
  public class UserEntry {
    public string Username { get; set; }
    public string Password { get; set; }

    public string UserId => Username;
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string UserMail { get; set; }
    public string PhoneNumber { get; set; }
    public string DisplayName { get; set; }
  }
}
