namespace Reveil.Backend.Interfaces {
  public interface IAuthenticationProviderFactory {
    IAuthenticationProvider Create();
  }
}
