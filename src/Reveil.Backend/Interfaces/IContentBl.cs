using CSharpFunctionalExtensions;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Interfaces {
  public interface IContentBl {
    Result<ContentEntryResponse, ErrorResponse> GetContent(string contentType, string languageCode);
    Result<ContentEntryResponse, ErrorResponse> AddOrUpdateContent(string contentType, string languageCode, string content);
  }
}
