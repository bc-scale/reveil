using CSharpFunctionalExtensions;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Interfaces {
  public interface IJournalBl {
    Result<ChildrenListResponse, ErrorResponse> GetJournalEntriesByCase(string caseId);
    Result<JournalEntryResponse, ErrorResponse> GetJournalEntry(string caseId, string journalId);
  }
}
