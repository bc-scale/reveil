using System;
using System.IO;
using System.Linq;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Reveil.Backend.Authentication;
using Reveil.Backend.BusinessLogic;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Middleware;
using Reveil.Backend.Middleware.Filter;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Configuration.Authentication;
using Reveil.Backend.Models.Configuration.Notification;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Repositories;

namespace Reveil.Backend {
  public class Startup {
    private IConfiguration Configuration { get; }
    private readonly ReveilConfiguration _reveilConfiguration;

    public Startup(IConfiguration configuration) {
      Configuration = configuration;
      _reveilConfiguration = Configuration.Get<ReveilConfiguration>();
    }

    public void ConfigureServices(IServiceCollection services) {
      services
        .Configure<ReveilConfiguration>(Configuration)
        .Configure<PersistenceConfiguration>(Configuration.GetSection("Persistence"))
        .Configure<NotificationConfiguration>(opts => NotificationConfigurationLogic.ConfigureNotificationConfiguration(Configuration, opts))
        .Configure<AuthenticationConfiguration>(opts => AuthenticationConfigurationLogic.ConfigureAuthenticationConfiguration(Configuration, opts));

      Directory.CreateDirectory(_reveilConfiguration.Persistence.DataRoot);

      AddPersistence(services);
      AddBusinessLogic(services);
      
      services
        .AddTransient<INotificationSenderFactory, NotificationSenderFactory>()
        .AddTransient<IAuthenticationProviderFactory, AuthenticationProviderFactory>();

      services.AddScoped<DevelopmentEnvironmentFilter>();
      services.AddSingleton<AuthorizationFilter>();

      services
        .AddControllers(c => c.Filters.Add<UnauthorizedAccessExceptionFilter>())
        .AddJsonOptions(o => o.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

      services.AddResponseCompression(options => {
        options.Providers.Add<GzipCompressionProvider>();
        options.Providers.Add<BrotliCompressionProvider>();
        options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[] {"image/svg+xml"});
      });

      services.AddSwaggerGen(c => {
        c.IncludeXmlComments(Path.Join(AppContext.BaseDirectory, "Reveil.Backend.xml"));
        c.SwaggerDoc("v1", new OpenApiInfo {
          Title = nameof(Reveil),
          Version = "v1", Contact = new OpenApiContact {
            Email = "reveil@rtrace.io",
            Name = "Reveil",
            Url = new Uri("https://reveil.rtrace.io")
          },
          Description = "Reveil is an F(L)OSS Whistleblower platform",
          License = new OpenApiLicense {Name = "SAL"}
        });

        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme {
          Name = "Bearer",
          Type = SecuritySchemeType.ApiKey,
          In = ParameterLocation.Header
        });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement {
          {
            new OpenApiSecurityScheme {
              Reference = new OpenApiReference {
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer"
              }
            },
            Array.Empty<string>()
          }
        });
      });
    }

    private static void AddPersistence(IServiceCollection services) {
      services.AddSingleton<IGenericRepository<UserEntity>, GenericFileSystemRepository<UserEntity>>();
      services.AddSingleton<IGenericRepository<CaseEntity>, GenericFileSystemRepository<CaseEntity>>();
      services.AddSingleton<IGenericRepository<FileEntity>, FileEntityFileSystemRepository>();
      services.AddSingleton<IGenericRepository<MessageEntity>, MessageEntityFileSystemRepository>();
      services.AddSingleton<IGenericRepository<JournalEntity>, JournalEntityFileSystemRepository>();
      services.AddSingleton<IGenericRepository<ContentEntity>, ContentEntityFileSystemPersistedRepository>();
      services.AddSingleton<IGenericRepository<AuthorizationEntity>, GenericFileSystemRepository<AuthorizationEntity>>();
    }

    private static void AddBusinessLogic(IServiceCollection services) {
      services.AddTransient<ICasesBl, CasesBl>();
      services.AddTransient<IUsersBl, UserBl>();
      services.AddTransient<IJournalBl, JournalBl>();
      services.AddTransient<IContentBl, ContentBl>();
      services.AddTransient<IMessagesBl, MessagesBl>();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
      if (env.IsDevelopment()) {
        app.UseDeveloperExceptionPage();
      }

      if (_reveilConfiguration.RedirectToHttps) {
        app.UseHttpsRedirection();
      }

      app.UseResponseCompression();
      var cspCollector = new ContentSecurityPolicyCollector(env.WebRootPath);
      app.UseSecurityHeaders(cspCollector);
      app.UseDefaultFiles();
      app.UseStaticFiles();
      app.UseRouting();

      if (_reveilConfiguration?.CrossOriginRequestOrigins?.Any() ?? false) {
        app.UseCors(c => {
          if ((bool)_reveilConfiguration?.CrossOriginRequestOrigins?.Any(s => s == "*")) {
            c.AllowAnyOrigin();
          } else {
            c
              .WithOrigins(_reveilConfiguration.CrossOriginRequestOrigins.ToArray())
              .AllowCredentials();
          }

          c.AllowAnyHeader().AllowAnyMethod().SetPreflightMaxAge(TimeSpan.FromHours(1));
        });
      }

      app.UseAuthorization();

      app.UseSwagger();
      app.UseSwaggerUI(c => {
        c.RoutePrefix = "swagger";
        c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{nameof(Reveil)} v1");
      });
      app.UseEndpoints(endpoints => {
        endpoints.MapControllers();
      });
    }
  }
}
