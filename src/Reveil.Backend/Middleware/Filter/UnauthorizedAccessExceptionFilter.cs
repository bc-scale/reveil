// ReSharper disable ClassNeverInstantiated.Global

using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Middleware.Filter {
  public class UnauthorizedAccessExceptionFilter : ExceptionFilterAttribute {
    public override void OnException(ExceptionContext context) {
      if (context.Exception is not UnauthorizedAccessException)
        return;

      context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
      context.Result = new JsonResult(new ErrorResponse {
        IsSuccess = false,
        DateTime = DateTime.UtcNow,
        StatusCode = HttpStatusCode.Unauthorized,
        Error = $"Unauthorized. Details: {context.Exception.Message}"
      });
    }

    public override Task OnExceptionAsync(ExceptionContext context) {
      OnException(context);
      return Task.CompletedTask;
    }
  }
}
