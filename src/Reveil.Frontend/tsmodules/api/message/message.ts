import * as api from '~/api'

import { IModelMessage, IModelMessagePayload, IModelMessageResponse } from './models'

import { RndGen } from '~/tsmodules/rndgen'
import { Base64 } from '~/tsmodules/base64'
import { ApiMgmt } from '~/tsmodules/api/mgmt'
import { ApiFile } from '~/tsmodules/api/file'

import { ApiEndpointExtensionLayer, ApiEndpointExtensionExtend } from '~/tsmodules/api/_eel'
import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'

export class ApiMessage extends ApiEndpointExtensionExtend {
    constructor(eel: ApiEndpointExtensionLayer) {
        super(eel)
    }

    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    // >!< message
    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    async MessageExists(case_id: string, message_id: string): Promise<boolean> {
        return new api.MessagesApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getMessage(case_id, message_id)
        .then(() => {
            return true;
        })
        .catch((e) => {
            return false;
        })
    }

    async MessageGet(case_id: string, message_id: string, load_files: boolean = false): Promise<IModelMessage> {
        // cache :: get
        var cache = ApiCache.Proxy<Array<IModelMessage>>(this.eel!.CacheType, "message", this.eel!)
        var cache_find = cache.GetCache()?.find((e) => e.response.caseId === case_id && e.response.id === message_id) || null
        if(cache_find !== null) {
            return cache_find
        }

        // api :: get
        return new api.MessagesApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getMessage(case_id, message_id)
        .then(async (e) => {
            var rtvl = {
                response: e.data,
                payload:  ApiCrypt.Proxy(ApiCrypt.ApiCryptTypeByIndex(e.data.dataProcessingMethodHint!), this.eel!).Decrypt(e.data.data!),
                modeltype: 'message',
                computed_files: load_files === true ? await new ApiFile(this.eel!).FileGetAll(case_id, message_id) : [],
                computed_author: await new ApiMgmt(this.eel!).MgmtGet(e.data.authorId!),
            } as IModelMessage

            // cache :: push
            cache.GetCache()?.push(rtvl)

            return rtvl;
        })
        .catch((e) => {
            throw Error('ModMessage => message_get')
        })
    }

    async MessageGetAll(case_id: string, load_files: boolean = false): Promise<Array<IModelMessage>> {
        return new api.MessagesApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getMessagesInCase(case_id)
        .then((e) => {
            var rtvl = []
            for(var message_id of e.data.ids || []) {
                rtvl.push(this.MessageGet(case_id, message_id, load_files)
                .then((e) => {
                    return e
                }))
            }
            return Promise.all(rtvl)
        })
        .then((e) => {
            e.sort((x, y) => ((x.response.creationDate || '') > (y.response.creationDate || '')) ? -1 : 1)
            return e
        })
        .catch((e) => {
            throw Error('ModMessage => message_getall')
        })
    }

    async MessagePost(case_id: string | null = null, message_id: string | null = null, data: IModelMessagePayload): Promise<IModelMessageResponse> {
        // case_id :: autogen uuid
        if(case_id === null) {
            case_id = new RndGen().UUIDv4();
        }
        // message_id :: autogen uuid
        if(message_id === null) {
            message_id = new RndGen().UUIDv4();
        }

        // crypt
        var payload = {
            data: await ApiCrypt.Proxy<IModelMessagePayload>(this.eel?.CryptType!, this.eel!).Encrypt(data),
            publicKey: null,
            dataProcessingMethodHint: this.eel?.CryptType.toString()
        } as api.CreateMessageRequest


        return new api.MessagesApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).createMessage(case_id, message_id, payload )
        .then((e) => {
            return {
                response: e.data,
                ref_obj: message_id,
            } as IModelMessageResponse
        })
        .catch((e) => {
            throw Error('ModMessage => message_post')
        })
    }
}
