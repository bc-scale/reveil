import { ApiFileMetaModsExif } from './mods/exif'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'


export class ApiFileMeta {
    // wipemeta :: facade
    static WipeMeta(file: File, content: string, inject: ApiEndpointExtensionLayer): string {
        if(file.name.endsWith('.jpg') || file.name.endsWith('.jpeg') || file.name.endsWith('.tiff')) {
            return ApiFileMetaModsExif.WipeMeta(file, content, inject)
        }

        return content
    }
}
