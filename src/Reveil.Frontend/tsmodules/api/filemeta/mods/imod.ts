import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

export class ApiFileMetaIMod {
    public static WipeMeta(file: File, content: string, inject: ApiEndpointExtensionLayer): string {
        throw Error("not implemented! This class is used as interface.")
    }
}
