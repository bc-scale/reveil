import * as api from '~/api'

import { IModelContent, IModelContentPayload } from './models'

import { ApiEndpointExtensionLayer, ApiEndpointExtensionExtend } from '~/tsmodules/api/_eel'
import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'

export class ApiContent extends ApiEndpointExtensionExtend {
    constructor(eel: ApiEndpointExtensionLayer) {
        super(eel)
    }

    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    // >!< content
    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    async ContentGet(name: string, language_code: string): Promise<IModelContent> {
        return new api.ContentApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getContent(name, language_code)
        .then((e) => {
            return {
                response: e.data,
                payload: {
                    data: e.data.content,
                }
            } as IModelContent
        })
        .catch((e) => {
            return {
                response: e.data,
                payload: {
                    data: '',
                }
            } as IModelContent
        })
    }

    async ContentSet(name: string, language_code: string, payload: IModelContentPayload): Promise<boolean> {
        var rqdata = {
            contentType: name,
            languageCode: language_code,
            content: payload.data,
        } as api.AddOrUpdateContentRequest
        
        return new api.ContentApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).addOrUpdateContent(rqdata)
        .then((e) => {
            return true;
        })
        .catch((e) => {
            throw Error("content_set");
        })
    }
}
