import * as api from '~/api'

import { IModelCase, IModelCaseResponse } from './models'

import { RndGen } from '~/tsmodules/rndgen'
import { ApiMessage } from '~/tsmodules/api/message'
import { ApiJournal } from '~/tsmodules/api/journal'

import { ApiEndpointExtensionLayer, ApiEndpointExtensionExtend } from '~/tsmodules/api/_eel'
import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'

export class ApiCase extends ApiEndpointExtensionExtend {
    constructor(eel: ApiEndpointExtensionLayer) {
        super(eel)
    }

    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    // >!< case
    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    async CaseExists(case_id: string): Promise<boolean> {
        return new api.CaseApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getCase(case_id)
        .then((e) => {
            return true;
        })
        .catch((e) => {
            return false;
        })
    }

    async CaseGet(case_id: string, load_messages: boolean = true, load_journals: boolean = false, load_files: boolean = false): Promise<IModelCase> {
        var _case = await new api.CaseApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getCase(case_id)
        var _messages = load_messages === true ? await new ApiMessage(this.eel!).MessageGetAll(case_id, load_files) : []
        var _journals = load_journals === true ? await new ApiJournal(this.eel!).JournalGetAll(case_id) : []

        return {
            id: case_id,

            response: _case.data,

            messages: _messages,
            journals: _journals,

            computed_state: _case.data.caseState?.toString()[0].toLowerCase(),
        } as IModelCase
    }

    async CaseGetAll(load_messages: boolean = true, load_journals: boolean = false, load_files: boolean = false, page_number: number = 0, page_size: number = 0): Promise<Array<IModelCase>> {
        return new api.CaseApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getAllCases()
        .then((e) => {
            var rtvl = [] as Array<Promise<IModelCase>>
            var case_ids = page_size === 0 ? e.data.ids! : e.data.ids!.slice((page_number * page_size) - page_size, (page_number * page_size + page_size) - page_size)
            for(var id of case_ids) {
                rtvl.push(this.CaseGet(id, load_messages, load_journals, load_files))
            }
            return Promise.all(rtvl)
        })
        .catch((e) => {
            throw Error('ModCase => case_getall')
        })
    }

    async CaseCount(): Promise<number> {
        return new api.CaseApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getAllCases()
        .then((e) => {
            return e.data.ids!.length
        })
        .catch((e) => {
            throw Error('ModCase => case_count')
        })
    }

    async CasePost(case_id: string | null = null): Promise<IModelCaseResponse> {
        // case_id :: already exists
        if(!!case_id && await this.CaseExists(case_id) === true) {
            throw Error('ModCase => case_post // case_already exists!');
        }

        // case_id :: autogen uuid
        if(!!case_id === false || case_id === null) {
            case_id = new RndGen().UUIDv4()
        }

        return new api.CaseApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).openCase(case_id)
        .then((e) => {
            return {
                response: e.data,
                ref_obj: case_id,
            } as IModelCaseResponse
        })
        .catch((e) => {
            throw Error('ModCase => case_post')
        })
    }

    async CaseState(case_id: string, case_state: string): Promise<boolean> {
        var _case_state = '' as api.CaseState;

        // open
        if(case_state === 'o' || case_state === 'open' || case_state === 'opened') {
            _case_state = api.CaseState.Open
        }

        // closed
        if(case_state === 'c' || case_state === 'close' || case_state === 'closed') {
            _case_state = api.CaseState.Closed
        }

        // archived
        if(case_state === 'a' || case_state === 'archive' || case_state === 'archived') {
            _case_state = api.CaseState.Archived
        }

        return new api.CaseApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).updateCaseState(case_id, _case_state)
        .then((e) => {
            return e.data.isSuccess === true
        })
        .catch((e) => {
            return false
        })
    }

    async CaseDelete(case_id: string): Promise<boolean> {
        return new api.CaseApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).deleteCase(case_id)
        .then((e) => {
            return e.data.isSuccess!
        })
        .catch((e) => {
            return false
        })
    }
}
