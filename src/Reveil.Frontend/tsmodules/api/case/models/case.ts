import * as api from '~/api'

import { IModelMessage } from '~/tsmodules/api/message/models'
import { IModelJournal } from '~/tsmodules/api/journal/models'

export interface IModelCase {
    id: string,

    response: api.CaseResponse,

    messages: Array<IModelMessage>,
    journals: Array<IModelJournal>,

    computed_state: string,
}
