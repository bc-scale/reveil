import { render } from 'mustache'

export class Template {
    RenderTemplate(template: string, data: any): string {
        return render(template, data)
    }
}
