import qrcode from 'qrcode'

export class QrCode {
    constructor() {
        
    }

    async GenerateDataUrl(data: string): Promise<string> {
        return await qrcode.toDataURL(data)
    }

    async GenerateSVG(data: string): Promise<string> {
        return await qrcode.toString(data, { type: 'svg'})
    }

    async GenerateText(data: string): Promise<string> {
        return await qrcode.toString(data, { type: 'utf8'})
    }
}
