import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'

import StoreCache from '~/store/cache'

let store_cache: StoreCache

function initialiseStores(store: Store<any>): void {
    store_cache = getModule(StoreCache, store)
}

export { initialiseStores, store_cache }
