import { Plugin, Context } from '@nuxt/types'

// >>> typing
interface ILocales {
    country_list(): Array<{ name: string, code: string }>
    language_list(): Array<{ name: string, code: string }>
}

// >>> data
export var data = function(context: Context): ILocales {
    return {
        country_list(): Array<{ name: string, code: string }> {
            return [
                { name: context.$transl8.t('#::country_austria'), code: 'at' },
                { name: context.$transl8.t('#::country_belgium'), code: 'be' },
                { name: context.$transl8.t('#::country_bulgaria'), code: 'bg' },
                { name: context.$transl8.t('#::country_croatia'), code: 'hr' },
                { name: context.$transl8.t('#::country_cyprus'), code: 'cy' },
                { name: context.$transl8.t('#::country_czechrepublic'), code: 'cz' },
                { name: context.$transl8.t('#::country_denmark'), code: 'dk' },
                { name: context.$transl8.t('#::country_estonia'), code: 'ee' },
                { name: context.$transl8.t('#::country_finland'), code: 'fi' },
                { name: context.$transl8.t('#::country_france'), code: 'fr' },
                { name: context.$transl8.t('#::country_germany'), code: 'de' },
                { name: context.$transl8.t('#::country_greece'), code: 'gr' },
                { name: context.$transl8.t('#::country_hungary'), code: 'hu' },
                { name: context.$transl8.t('#::country_ireland'), code: 'ie' },
                { name: context.$transl8.t('#::country_italy'), code: 'it' },
                { name: context.$transl8.t('#::country_latvia'), code: 'lv' },
                { name: context.$transl8.t('#::country_lithuania'), code: 'lt' },
                { name: context.$transl8.t('#::country_luxembourg'), code: 'lu' },
                { name: context.$transl8.t('#::country_malta'), code: 'mt' },
                { name: context.$transl8.t('#::country_netherlands'), code: 'nl' },
                { name: context.$transl8.t('#::country_poland'), code: 'pl' },
                { name: context.$transl8.t('#::country_portugal'), code: 'pt' },
                { name: context.$transl8.t('#::country_romania'), code: 'ro' },
                { name: context.$transl8.t('#::country_slovakia'), code: 'sk' },
                { name: context.$transl8.t('#::country_slovenia'), code: 'sl' },
                { name: context.$transl8.t('#::country_spain'), code: 'es' },
                { name: context.$transl8.t('#::country_sweden'), code: 'se' },
            ]
        },
        language_list(): Array<{ name: string, code: string }> {
            return [
                // { name: context.$transl8.t('#::language_bulgarian'), code: 'bg' },
                // { name: context.$transl8.t('#::language_croatian'), code: 'hr' },
                // { name: context.$transl8.t('#::language_czech'), code: 'cs' },
                // { name: context.$transl8.t('#::language_danish'), code: 'da' },
                // { name: context.$transl8.t('#::language_dutch'), code: 'nl' },
                { name: context.$transl8.t('#::language_english'), code: 'en' },
                // { name: context.$transl8.t('#::language_estonian'), code: 'et' },
                // { name: context.$transl8.t('#::language_finnish'), code: 'fi' },
                // { name: context.$transl8.t('#::language_french'), code: 'fr' },
                { name: context.$transl8.t('#::language_german'), code: 'de' },
                // { name: context.$transl8.t('#::language_greek'), code: 'el' },
                // { name: context.$transl8.t('#::language_hungarian'), code: 'hu' },
                // { name: context.$transl8.t('#::language_irish'), code: 'ga' },
                // { name: context.$transl8.t('#::language_italian'), code: 'it' },
                // { name: context.$transl8.t('#::language_latvian'), code: 'lv' },
                // { name: context.$transl8.t('#::language_lithuanian'), code: 'lt' },
                // { name: context.$transl8.t('#::language_maltese'), code: 'mt' },
                // { name: context.$transl8.t('#::language_polish'), code: 'pl' },
                // { name: context.$transl8.t('#::language_portuguese'), code: 'pt' },
                // { name: context.$transl8.t('#::language_romanian'), code: 'ro' },
                // { name: context.$transl8.t('#::language_slovak'), code: 'sk' },
                // { name: context.$transl8.t('#::language_slovenian'), code: 'sl' },
                // { name: context.$transl8.t('#::language_spanish'), code: 'es' },
                // { name: context.$transl8.t('#::language_swedish'), code: 'sv' },
            ]
        },
    }
}

declare module 'vue/types/vue' {
    interface Vue {
        $locales: ILocales
    }
}

declare module '@nuxt/types' {
    interface NuxtAppOptions {
        $locales: ILocales
    }
    
    interface Context {
        $locales: ILocales
    }
}

const locales: Plugin = (context, inject) => {
    inject('locales', data(context))
}

export default locales
